export default (state = {}, action) => {
 switch (action.type) {
  case 'ADD_USER_ACTION':
   return {
    userCreated: true
   }
  default:
   return state
 }
}