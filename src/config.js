export default {
  s3: {
    REGION: "eu-west-1",
    BUCKET: "quiz-awmachine-api-prod-serverlessdeploymentbucke-1wefrnidqk6y8"
  },
  apiGateway: {
    REGION: "eu-west-1",
    URL: "https://687ag0dwe0.execute-api.eu-west-1.amazonaws.com/prod"
  },
  cognito: {
    REGION: "eu-west-1",
    USER_POOL_ID: "eu-west-1_65fwM87qN",
    APP_CLIENT_ID: "54rtv4ccountlp22ove7g4pphp",
    IDENTITY_POOL_ID: "eu-west-1:3d9412f7-6973-47ca-b9b4-3852fd07c522"
  }
};
