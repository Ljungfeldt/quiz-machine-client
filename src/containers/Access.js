import React, { Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "./Access.css";
import { API } from "aws-amplify";
import { connect } from 'react-redux';
import { addUserAction } from '../actions/AddUserAction';

export class Access extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userName: "",
      quizName: "graceland"
    };
  }

  validateForm() {
    return this.state.userName.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit = async event => {
    event.preventDefault();

    this.setState({ isLoading: true });

    try {
      await this.createPlayer({
        content: this.state.content
      });
      this.props.history.push("/");
      this.props.addUserAction(true);
    } catch (e) {
      alert(e);
      this.setState({ isLoading: false });
    }
  }

  createPlayer() {
    return API.post("players", "/player", {
      body: { name: this.state.userName, quizname: this.state.quizName }
    });
  }

  render() {
    return (
      <div className="Login">
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="userName" bsSize="large">
            <ControlLabel>Player Name</ControlLabel>
            <FormControl
              autoFocus
              type="string"
              value={this.state.userName}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="quizName" bsSize="large">
            <ControlLabel>Quiz Name</ControlLabel>
            <FormControl
              autoFocus
              type="string"
              value={this.state.quizName}
              onChange={this.handleChange}
            />
          </FormGroup>
          <Button
            block
            bsSize="large"
            disabled={!this.validateForm()}
            type="submit"
          >
            Play!
          </Button>
        </form>
      </div>
    );
  }
}


const mapStateToProps = state => ({
  ...state
 })

const mapDispatchToProps = dispatch => ({
  addUserAction: () => dispatch(addUserAction())
})

export default connect(mapStateToProps, mapDispatchToProps)(Access);
