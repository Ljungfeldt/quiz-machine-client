import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Link } from "react-router-dom";
import { Navbar } from "react-bootstrap";
import Routes from "./Routes";
import { connect } from 'react-redux';
import { addUserAction } from './actions/AddUserAction';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
    
    };
  }

addUser = (event) => {
    this.props.addUserAction();
   }

  render() {
    const childProps = {
      userCreated: this.state.userCreated,
      userIsCreated: this.userIsCreated
    };
    
    return (
      <div className="App">
        {this.props.createUserReducer.userCreated
          ? <div>Logout</div>
          : <div></div>
        }
        <Routes childProps={childProps} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ...state
 })

const mapDispatchToProps = dispatch => ({
  addUserAction: () => dispatch(addUserAction())
})

export default connect(mapStateToProps, mapDispatchToProps)(App);
